- Heal
  - HealType
  - BaseHeal
  - Modifiers

- Damage
  - DamageType
  - BaseDamge
  - Modifiers

- Effect
  - TargetType
  - Damages (List of Damages)
  - Heals (List of Heals)
  - #Balances (List of Balance Changes)
  - #Statuses (List of Status Changes)

- Action
  - Requirements (List of Requirements)
  - Effects (List of Effects)

- Card
  - Play    (Action)
  - Discard (Action)

- Deck
  - Hand (List of Cards)
  - Pile (List of Cards)

- Equipment

- Trait

- Characteristic

- Stats

- Character
  - Deck (Deck)
  - BaseHealth
  - CurrentHealth

- Team
  - Members (List of Characters)

- Alliance
  - Teams (List of Teams)

- Game
  - Alliances (List of Alliances)