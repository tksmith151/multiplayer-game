import enum
import random


class DamageType(enum.Enum):
    FIRE = 0
    WATER = 1
    AIR = 2
    EARTH = 3
    LIGHT = 4
    DARK = 5
    PHYSICAL = 6


class TargetType(enum.Enum):
    ENEMY = -2
    OPONENT = -1
    SELF = 0
    ALLY = 1
    COMPANIONS = 3
    TEAM = 4


class Damage:
    def __init__(self, damage_type, base_damage, damage_modifiers) -> None:
        self.damage_type = damage_type
        self.base_damage = base_damage
        self.damage_modifiers = damage_modifiers


class Effect:
    def __init__(self, target_type) -> None:
        self.target_type = target_type


class Action:
    def __init__(self, action_points, effects) -> None:
        self.action_points = action_points
        self.effects = effects


class Card:
    def __init__(self, play, discard) -> None:
        self.play = play
        self.discard = discard


class Deck:
    def __init__(self, cards) -> None:
        self.cards = cards.copy()
        self.left = None
        self.right = None
        self.shuffle()
        self.draw_left()
        self.draw_right()

    def _draw(self):
        return self.cards.pop()

    def shuffle(self):
        random.shuffle(self.cards)

    def draw_left(self):
        self.left = self._draw()

    def draw_right(self):
        self.right = self._draw()

    def discard_left(self):
        self.cards.insert(0, self.left)
        self.left = self.cards.pop()

    def discard_right(self):
        self.cards.insert(0, self.right)
        self.right = self.cards.pop()


class Character:
    def __init__(self, deck, max_health) -> None:
        self.deck = deck
        self.max_health = max_health
        self.current_health = self.max_health


class Team:
    def __init__(self, left, mid, right) -> None:
        self.left = left
        self.mid = mid
        self.right = right


class Game:
    def __init__(self, left, right) -> None:
        self.left = left
        self.right = right
