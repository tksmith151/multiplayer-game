import unittest

import mock_manager as manager

import src.authentication.handler as handler


class TestHandler(unittest.TestCase):
    def __init__(self):
        config = {}
        self.handler = handler.Handler(manager.Manager(), config)

    def test_empty_request(self):
        request = {}
        response = self.handler.handle(request)


if __name__ == "__main__":
    unittest.main()
