import unittest
from src.functions.parse_authenticate_password_request import (
    parse_authenticate_password_request,
)


class TestParseAuthenticatePasswordRequest(unittest.TestCase):
    def test_unpack_valid_request(self):
        valid_request = [0, "user", "password"]
        user, password = parse_authenticate_password_request(valid_request)
        self.assertEqual(user, "user")
        self.assertEqual(password, "password")


if __name__ == "__main__":
    unittest.main()
