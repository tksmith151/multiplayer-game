import unittest
from src.enums.hash_type import HashType
from src.functions.verify_password import verify_password


class TestHashPassword(unittest.TestCase):
    def test_correct_password(self):
        failed_message = "Failed on correct password."
        password = "pleaseletmein"
        verifier = {
            "type": HashType.SCRYPT,
            "salt": bytes("SodiumChloride", "utf-8").hex(),
            "N": 16384,
            "r": 8,
            "p": 1,
            "hash": "7023bdcb3afd7348461c06cd81fd38ebfda8fbba904f8e3ea9b543f6545da1f2d5432955613f0fcf62d49705242a9af9e61e85dc0d651e40dfcf017b45575887",
        }
        correct_password = verify_password(password, verifier)
        self.assertTrue(correct_password, failed_message)

    def test_incorrect_password(self):
        failed_message = "Failed on incorrect password."
        password = "!pleaseletmein"
        verifier = {
            "type": HashType.SCRYPT,
            "salt": bytes("SodiumChloride", "utf-8").hex(),
            "N": 16384,
            "r": 8,
            "p": 1,
            "hash": "7023bdcb3afd7348461c06cd81fd38ebfda8fbba904f8e3ea9b543f6545da1f2d5432955613f0fcf62d49705242a9af9e61e85dc0d651e40dfcf017b45575887",
        }
        incorrect_password = verify_password(password, verifier)
        self.assertFalse(incorrect_password, failed_message)


if __name__ == "__main__":
    unittest.main()
