import unittest
from src.enums.hash_type import HashType
from src.functions.hash_password import hash_password


class TestHashPassword(unittest.TestCase):
    def test_scrypt_spec_first_example(self):
        password = ""
        hash_parameters = {
            "type": HashType.SCRYPT,
            "salt": "",
            "N": 16,
            "r": 1,
            "p": 1,
        }
        password_hash = hash_password(password, hash_parameters)
        self.assertEqual(
            password_hash,
            "77d6576238657b203b19ca42c18a0497f16b4844e3074ae8dfdffa3fede21442fcd0069ded0948f8326a753a0fc81f17e8d3e0fb2e0d3628cf35e20c38d18906",
        )

    def test_scrypt_spec_second_example(self):
        password = "password"
        hash_parameters = {
            "type": HashType.SCRYPT,
            "salt": bytes("NaCl", "utf-8").hex(),
            "N": 1024,
            "r": 8,
            "p": 16,
        }
        password_hash = hash_password(password, hash_parameters)
        self.assertEqual(
            password_hash,
            "fdbabe1c9d3472007856e7190d01e9fe7c6ad7cbc8237830e77376634b3731622eaf30d92e22a3886ff109279d9830dac727afb94a83ee6d8360cbdfa2cc0640",
        )

    def test_scrypt_spec_third_example(self):
        password = "pleaseletmein"
        hash_parameters = {
            "type": HashType.SCRYPT,
            "salt": bytes("SodiumChloride", "utf-8").hex(),
            "N": 16384,
            "r": 8,
            "p": 1,
        }
        password_hash = hash_password(password, hash_parameters)
        self.assertEqual(
            password_hash,
            "7023bdcb3afd7348461c06cd81fd38ebfda8fbba904f8e3ea9b543f6545da1f2d5432955613f0fcf62d49705242a9af9e61e85dc0d651e40dfcf017b45575887",
        )


if __name__ == "__main__":
    unittest.main()
