import unittest
from src.enums.authentication_request_type import AuthenticationRequestType
from src.functions.get_request_type import get_request_type


class TestUnpackAuthenticatePasswordRequest(unittest.TestCase):
    def test_get_valid_request_type(self):
        valid_request = [0, "user", "password"]
        request_type = get_request_type(valid_request, AuthenticationRequestType)
        self.assertEqual(request_type, AuthenticationRequestType(0))


if __name__ == "__main__":
    unittest.main()
