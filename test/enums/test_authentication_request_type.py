import unittest
from src.enums.authentication_request_type import AuthenticationRequestType


class TestAuthenticatePassword(unittest.TestCase):
    def test_hash_type_scrypt(self):
        self.assertEqual(
            AuthenticationRequestType.AUTHENTICATE_PASSWORD,
            AuthenticationRequestType(0),
        )


if __name__ == "__main__":
    unittest.main()
