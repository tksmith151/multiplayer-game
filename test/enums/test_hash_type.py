import unittest
from src.enums.hash_type import HashType


class TestScrypt(unittest.TestCase):
    def test_hash_type_scrypt(self):
        self.assertEqual(HashType.SCRYPT, HashType(0))


if __name__ == "__main__":
    unittest.main()
