import enum


class AuthenticationRequestType(enum.Enum):
    AUTHENTICATE_PASSWORD = 0
