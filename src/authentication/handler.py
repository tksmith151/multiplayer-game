from src.enums.authentication_request_type import AuthenticationRequestType

from src.functions.get_request_type import get_request_type
from src.functions.parse_authenticate_password_request import (
    parse_authenticate_password_request,
)
from src.functions.verify_password import verify_password


class Handler:
    def __init__(self, manager, config):
        self.manager = manager

    def handle(self, request):
        authentication_request_type = get_request_type(
            request, AuthenticationRequestType
        )
        if (
            authentication_request_type
            == AuthenticationRequestType.AUTHENTICATE_PASSWORD
        ):
            self.__handle_authenticate_password(request)
        return

    def __handle_authenticate_password(self, request):
        user, password = parse_authenticate_password_request(request)
        salt, hash = self.manager.get_user_password_verifiers(user)
        if verify_password(password, salt, hash):
            return True
        else:
            return False
