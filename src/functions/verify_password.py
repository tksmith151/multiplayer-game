from src.enums.hash_type import HashType
from .hash_password import hash_password


def verify_password(password, verifier):
    hash_parameters = {}
    if verifier["type"] == HashType.SCRYPT:
        hash_parameters = {
            "type": HashType.SCRYPT,
            "salt": verifier["salt"],
            "N": verifier["N"],
            "r": verifier["r"],
            "p": verifier["p"],
        }
    password_hash = hash_password(password, hash_parameters)
    if password_hash == verifier["hash"]:
        return True
    return False
