import hashlib
from src.enums.hash_type import HashType

# Constants based on NIST standards September 2021
# N = 32768  # 2 ^ 15, computational hardness factor
# R = 8  # Block size for memory hardness factor
# P = 1  # Parellelization factor


def hash_password(password, hash_parameters):
    password_bytes = bytes(password, "utf-8")
    if hash_parameters["type"] == HashType.SCRYPT:
        hash_bytes = hashlib.scrypt(
            password_bytes,
            salt=bytes.fromhex(hash_parameters["salt"]),
            n=hash_parameters["N"],
            r=hash_parameters["r"],
            p=hash_parameters["p"],
        )
        return hash_bytes.hex()
